package client

import (
	"os"
	"testing"
)

func TestBuildClient(t *testing.T) {
	var tests = []struct {
		caFile string
	}{
		{"cert_testfile"},
	}

	for _, test := range tests {

		_, err := buildHTTPClient(test.caFile)

		if err != nil {
			t.Errorf("Got error: %v", err)
		}

	}
}

func TestNewKubernetesClient(t *testing.T) {
	var tests = []struct {
		k8sURL     string
		hostEnvVar string
		portEnvVar string
		caFile     string
		tokenFile  string
		want       string
		wantErr    bool
	}{
		{
			"http://127.0.0.1:8001",
			"",
			"",
			"cert_testfile",
			"token_testfile",
			"http://127.0.0.1:8001",
			false,
		},
		{
			"http://127.0.0.1:8001",
			"10.3.0.4",
			"443",
			"cert_testfile",
			"token_testfile",
			"http://127.0.0.1:8001",
			false,
		},
		{
			"",
			"10.3.0.4",
			"443",
			"cert_testfile",
			"token_testfile",
			"https://10.3.0.4:443",
			false,
		},
		{
			"",
			"10.3.0.4",
			"",
			"cert_testfile",
			"token_testfile",
			"",
			true,
		},
		{
			"",
			"",
			"443",
			"cert_testfile",
			"token_testfile",
			"",
			true,
		},
		{
			"",
			"",
			"",
			"cert_testfile",
			"token_testfile",
			"",
			true,
		},
	}

	for _, test := range tests {

		if test.hostEnvVar != "" {
			os.Setenv(svcHostEnvVar, test.hostEnvVar)
		}
		if test.portEnvVar != "" {
			os.Setenv(svcPortEnvVar, test.portEnvVar)
		}

		c, err := NewKubernetesClient(test.k8sURL, test.caFile, test.tokenFile)

		os.Unsetenv(svcPortEnvVar)
		os.Unsetenv(svcHostEnvVar)

		if err != nil {
			if !test.wantErr {
				t.Errorf("Got error: %v", err)
			}

			continue
		}

		if test.wantErr {
			t.Errorf(
				"No error for %q %q %q",
				test.k8sURL,
				test.hostEnvVar,
				test.portEnvVar,
			)
		}

		if c.k8sURL != test.want {
			t.Errorf("Got %q, want %q", c.k8sURL, test.want)
		}

	}
}
