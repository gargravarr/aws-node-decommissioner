package client

// NodeList holds the data returned when requesting the list of nodes
// for a cluster.
type NodeList struct {
	Items []Node `json:"items"`
}

// Node contains the metadata information of a node.
// nodes.
type Node struct {
	Metadata Metadata `json:"metadata"`
}

// Metadata contains the node's name and labels.
type Metadata struct {
	Name   string            `json:"name,omitempty"`
	Labels map[string]string `json:"labels,omitempty"`
}
