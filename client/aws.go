package client

import (
	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/aws/session"
	"github.com/aws/aws-sdk-go/service/ec2"
	log "github.com/sirupsen/logrus"
)

const (
	// DefaultAWSRegion is the default Region used by the AWS client.
	DefaultAWSRegion = "eu-central-1"
)

// AWS executes HTTP requests to the AWS API.
type AWS struct {
	session *session.Session
}

// NewAWSClient creates a new client targeted to the specified region of
// the current AWS account.
func NewAWSClient(region string) (*AWS, error) {
	s, err := session.NewSession(aws.NewConfig().WithRegion(region))
	if err != nil {
		return nil, err
	}

	return &AWS{session: s}, nil
}

// GetInstancesToRetire returns the list of EC2 instances with scheduled
// events.
func (c *AWS) GetInstancesToRetire() ([]string, error) {
	svc := ec2.New(c.session)

	ids := []*string{}

	err := svc.DescribeInstanceStatusPages(
		&ec2.DescribeInstanceStatusInput{},
		func(page *ec2.DescribeInstanceStatusOutput, lastPage bool) bool {
			for _, item := range page.InstanceStatuses {
				if len(item.Events) > 0 {
					ids = append(ids, item.InstanceId)
					log.Debugf(
						"Instance %s has scheduled events: %v",
						*item.InstanceId,
						item.Events,
					)
				}
			}

			return true
		},
	)

	if err != nil {
		return nil, err
	}

	if len(ids) == 0 {
		return nil, nil
	}

	inputFilter := &ec2.DescribeInstancesInput{
		InstanceIds: ids,
	}

	var names []string

	err = svc.DescribeInstancesPages(
		inputFilter,
		func(page *ec2.DescribeInstancesOutput, lastPage bool) bool {
			for _, rsv := range page.Reservations {
				for _, inst := range rsv.Instances {
					names = append(names, *inst.PrivateDnsName)
				}
			}

			return true
		},
	)

	if err != nil {
		return nil, err
	}

	return names, nil
}
