package client

import (
	"os"

	log "github.com/sirupsen/logrus"
)

type token []byte

func newCredentials(tokenPath string) (token, error) {

	data, err := os.ReadFile(tokenPath)
	if err != nil {
		log.Debugf("Failed to read token in file %s: %v", data, err)
		return nil, err
	}

	return token(data), nil
}
