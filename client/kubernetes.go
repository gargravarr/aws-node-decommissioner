package client

import (
	"bytes"
	"crypto/tls"
	"crypto/x509"
	"encoding/json"
	"errors"
	"fmt"
	"net"
	"net/http"
	"os"
	"time"

	log "github.com/sirupsen/logrus"
)

const (
	nodesResource = "/api/v1/nodes"
	svcHostEnvVar = "KUBERNETES_SERVICE_HOST"
	svcPortEnvVar = "KUBERNETES_SERVICE_PORT"

	// DefaultDir is the default path for the token and certificate
	// files.
	DefaultDir = "/var/run/secrets/kubernetes.io/serviceaccount/"
	// DefaultTokenFile is the default file with the bearer token used
	// in requests to the Kubernetes API server.
	DefaultTokenFile = DefaultDir + "token"
	// DefaultCAFile is the default root certificate used by requests
	// with a bearer token, sent to the Kubernetes API Server.
	DefaultCAFile = DefaultDir + "ca.crt"
)

// Kubernetes executes HTTP requests to the Kubernetes API Server.
type Kubernetes struct {
	credentials token
	httpClient  *http.Client
	k8sURL      string
}

func buildHTTPClient(caFile string) (*http.Client, error) {

	transport := &http.Transport{
		DialContext: (&net.Dialer{
			Timeout:   10 * time.Second,
			KeepAlive: 30 * time.Second,
			DualStack: true,
		}).DialContext,
		TLSHandshakeTimeout:   10 * time.Second,
		ResponseHeaderTimeout: 10 * time.Second,
		ExpectContinueTimeout: 30 * time.Second,
		MaxIdleConns:          5,
		MaxIdleConnsPerHost:   5,
	}

	if caFile != "" {
		rootCA, err := os.ReadFile(caFile)
		if err != nil {
			return nil, err
		}

		certPool := x509.NewCertPool()
		if !certPool.AppendCertsFromPEM(rootCA) {
			return nil, errors.New("invalid certificate")
		}

		transport.TLSClientConfig = &tls.Config{
			MinVersion: tls.VersionTLS12,
			RootCAs:    certPool,
		}
	}

	return &http.Client{
		Transport: transport,
	}, nil
}

// NewKubernetesClient creates a new HTTP client pointing to the
// Kubernetes API server specified by k8sURL.
func NewKubernetesClient(k8sURL, caFile, tokenFile string) (
	*Kubernetes,
	error,
) {
	if k8sURL == "" {
		host, ok := os.LookupEnv(svcHostEnvVar)
		if !ok {
			return nil, fmt.Errorf("%s is not set", svcHostEnvVar)
		}

		port, ok := os.LookupEnv(svcPortEnvVar)
		if !ok {
			return nil, fmt.Errorf("%s is not set", svcPortEnvVar)
		}

		k8sURL = fmt.Sprintf("https://%s:%s", host, port)
	}

	httpClient, err := buildHTTPClient(caFile)
	if err != nil {
		return nil, err
	}

	c := &Kubernetes{
		httpClient: httpClient,
		k8sURL:     k8sURL,
	}

	c.credentials, err = newCredentials(tokenFile)
	if err != nil {
		return nil, err
	}

	return c, nil
}

// GetMatchingNodes returns the nodes from the cluster matching the
// specified instance list.
//
// If labelFilter is set, only nodes with the specified labels are matched.
func (c *Kubernetes) GetMatchingNodes(
	instNames []string,
	labelFilter map[string]string,
) ([]string, error) {

	req, err := http.NewRequest(http.MethodGet, c.k8sURL+nodesResource, nil)
	if err != nil {
		return nil, err
	}

	q := req.URL.Query()
	for k, v := range labelFilter {
		q.Add(k, v)
	}
	req.URL.RawQuery = q.Encode()

	req.Header.Set("Authorization", "Bearer "+string(c.credentials))

	log.Debugf("Requesting nodes from %s", c.k8sURL)
	rsp, err := c.httpClient.Do(req)
	if err != nil {
		return nil, err
	}

	defer rsp.Body.Close()

	if rsp.StatusCode != http.StatusOK {
		return nil, fmt.Errorf(
			"request failed, status: %d, %s",
			rsp.StatusCode,
			rsp.Status,
		)
	}

	var nodeList NodeList
	if err = json.NewDecoder(rsp.Body).Decode(&nodeList); err != nil {
		return nil, err
	}

	var foundNodes []string
	for _, node := range nodeList.Items {
		for _, item := range instNames {
			if item == node.Metadata.Name {
				foundNodes = append(foundNodes, item)
				log.Debugf("Instance %s is a node", item)
			}
		}
	}

	return foundNodes, nil
}

// LabelNode labels the specified node with the provided key/value pair.
//
// No labelling is done if dryRun is set.
func (c *Kubernetes) LabelNode(node, key, value string, dryRun bool) error {
	patch := &Node{
		Metadata: Metadata{
			Labels: map[string]string{key: value},
		},
	}

	jsonPatch, err := json.Marshal(patch)
	if err != nil {
		return err
	}

	req, err := http.NewRequest(
		http.MethodPatch,
		c.k8sURL+nodesResource+"/"+node,
		bytes.NewBuffer(jsonPatch),
	)
	if err != nil {
		return err
	}

	req.Header.Set("Authorization", "Bearer "+string(c.credentials))
	req.Header.Set("Accept", "application/json")
	req.Header.Set("Content-Type", "application/merge-patch+json")

	log.Debugf("Label node %s with %s=%s", node, key, value)

	if dryRun {
		log.Debugf("Dry run. request not sent.")
		return nil
	}

	rsp, err := c.httpClient.Do(req)
	if err != nil {
		return err
	}
	defer rsp.Body.Close()

	if rsp.StatusCode != http.StatusOK {
		return fmt.Errorf(
			"request failed, status: %d, %s",
			rsp.StatusCode,
			rsp.Status,
		)
	}

	return nil
}
