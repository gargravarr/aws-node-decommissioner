# AWS Node Decommissioner

Marks EC2 instances used as nodes in a Kubernetes cluster as eligible for
decommissionig, based on relevant infra-structure conditions.

One example of a relevant condition is when AWS schedules events on EC2
instances - see [documentation][EC2 Scheduled Events Docs].

Marking as eligible for decommissioning is done by adding a label to the
relevant node.

[EC2 Scheduled Events Docs]: https://docs.aws.amazon.com/AWSEC2/latest/UserGuide/monitoring-instances-status-check_sched.html#viewing_scheduled_events

## Usage

The application can be executed in a `CronJob` running periodically inside a
Kubernetes cluster. With the appropriate permissions it can also be executed
outside of the cluster.

## Conditions

- [x] Check for scheduled maintenance of EC2 instances;
