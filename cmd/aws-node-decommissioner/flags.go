package main

import (
	"gitlab.com/gargravarr/aws-node-decommissioner/client"

	log "github.com/sirupsen/logrus"
	"github.com/spf13/cobra"
)

var (
	caFile      string
	debug       bool
	dryRun      bool
	k8sURL      string
	labelKey    string
	labelValue  string
	labelFilter map[string]string
	noCA        bool
	region      string
	tokenFile   string
)

func init() {
	cobra.OnInitialize(initLogging)

	rootCmd.Flags().StringVar(
		&caFile,
		"ca-file",
		client.DefaultCAFile,
		"certificate file",
	)

	rootCmd.Flags().BoolVar(
		&debug,
		"debug",
		false,
		"set log level to debug",
	)

	rootCmd.Flags().BoolVar(
		&dryRun,
		"dry-run",
		false,
		"don't terminate nodes. Just log what would be done",
	)

	rootCmd.Flags().StringVar(
		&k8sURL,
		"k8s-url",
		"",
		"kubernetes API URL. Defaults to the internal Kubernetes service.",
	)

	rootCmd.Flags().StringVar(
		&labelKey,
		"key",
		"lifecycle-status",
		"label key",
	)

	rootCmd.Flags().BoolVar(
		&noCA,
		"no-ca",
		false,
		"don't use certificate when connecting to the Kubernetes API server",
	)

	rootCmd.PersistentFlags().StringVar(
		&region,
		"region",
		client.DefaultAWSRegion,
		"AWS region",
	)

	rootCmd.PersistentFlags().StringVar(
		&tokenFile,
		"token-file",
		client.DefaultTokenFile,
		"token file, used to authenticate requests to the API server",
	)

	rootCmd.Flags().StringVar(
		&labelValue,
		"value",
		"decommission-pending",
		"label value",
	)

	rootCmd.Flags().StringToStringVar(
		&labelFilter,
		"label-filter",
		map[string]string{"lifecycle-status": "ready"},
		"filter nodes by labels",
	)
}

// initLogging configures the logging level as specified by the --debug
// flag.
func initLogging() {
	if debug {
		log.SetLevel(log.DebugLevel)
	}
}
