package main

import (
	"gitlab.com/gargravarr/aws-node-decommissioner/client"

	log "github.com/sirupsen/logrus"
	"github.com/spf13/cobra"
)

// rootCmd represents the base command when called without any
// subcommands
var rootCmd = &cobra.Command{
	Use:   "aws-node-decommissioner",
	Short: "Labels worker nodes with scheduled events.",
	Long: `Checks for scheduled maintenance events in EC2 instances, in the
current AWS API. If a maintenance event is scheduled, terminates the instance.
`,
	Run: labelNodes,
}

func labelNodes(cmd *cobra.Command, args []string) {
	if noCA {
		caFile = ""
	}

	k8sClient, err := client.NewKubernetesClient(k8sURL, caFile, tokenFile)
	if err != nil {
		log.Fatalf("error: %v", err)
	}

	awsClient, err := client.NewAWSClient(region)
	if err != nil {
		log.Fatalf("error: %v", err)
	}

	insts, err := awsClient.GetInstancesToRetire()
	if err != nil {
		log.Fatalf("error: %v", err)
	}
	log.Debugf("EC2 instance names: %v", insts)

	if len(insts) == 0 {
		log.Infof("No EC2 instances scheduled for termination.")
		return
	}

	nodes, err := k8sClient.GetMatchingNodes(insts, labelFilter)
	if err != nil {
		log.Fatalf("error: %v", err)
	}

	if len(nodes) == 0 {
		log.Infof("No nodes scheduled for termination.")
		return
	}

	log.Infof("Scheduled events found on: %v", nodes)

	for _, item := range nodes {
		err = k8sClient.LabelNode(item, labelKey, labelValue, dryRun)
		if err != nil {
			log.Fatalf("error: %v", err)
		}
	}
}

func main() {
	if err := rootCmd.Execute(); err != nil {
		log.Fatalf("%v", err)
	}
}
